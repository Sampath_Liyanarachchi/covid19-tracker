import React from "react";

import Cards from "./components/Cards/Cards";
import Charts from "./components/Charts/Charts";
import CountryPicker from "./components/CountryPicker/CountryPicker";
import { fetchData } from "./api/covidAPI";

import "./App.css";

class App extends React.Component {
  state = {
    data: {},
    lankaData: {},
    country: "",
  };

  async componentDidMount() {
    const fetchedData = await fetchData();
    const fetchLankaData = await fetchData("Sri Lanka");

    this.setState({ data: fetchedData });
    this.setState({ lankaData: fetchLankaData });
  }

  handleCountryChange = async (country) => {
    const fetchedData = await fetchData(country);

    this.setState({ data: fetchedData, country: country });
  };
  render() {
    console.log("Global", this.state.data);
    console.log("country", this.state.lankaData);
    console.log("selected", this.state.country);
    const { data, country, lankaData } = this.state;

    return (
      <div className="container">
        <h1>Sri Lankan Covid-19 Data</h1>
        <Cards data={lankaData} />
        <h1>{(country) ? (country) : "Global Total"} Covid-19 Data</h1>
        <Cards data={data} />
        <CountryPicker handleCountryChange={this.handleCountryChange} />
        <Charts data={data} country={country} />
      </div>
    );
  }
}

export default App;
